import {
    isNumber,
    isString
} from "util";

import Scene from "./superClasses/scene";
import TestScene from "./scenes/testScene";

import MyLoader from "./myLoader";
import UiScene from "./superClasses/uiScene";


export default class ScenesManager {

    constructor(game, renderer, canvas, physicsStats, isDebug = false) {

        this.game = game;
        this.game.scenesManager = this;
        this.renderer = renderer;
        this.canvas = canvas;
        this.physicsStats = physicsStats;
        this.isDebug = isDebug;

        //all scenes of a game; 
        this.scenes = {
            "TestScene": TestScene,
        };

        //active scene reference
        this.currentScene = null;

        this.loader = new MyLoader();

        // creating a new preloader
        this.createPreloader();

        //game starts with the first scene in this.scenes object 
        this.start(); 
    }

    createPreloader(){
        this.preloader = new THREE.BallSpinerLoader({ groupRadius:15 });
        this.preloader.mesh.name = "preloader";
        this.preloader.mesh.position.x = 0;
        this.preloader.mesh.position.y = 15;
        this.preloader.mesh.rotation.x = -0.38;
        //this.preloader.mesh.rotation.y = 0.3;
    }

    //shows first scene in this.scenes object 
    start() {
        this.showScene(0);
    }

    //takes string with scene name or scene index
    async showScene(scene) {
        let sceneName = null;
        //try to define desired scene name
        if (isNumber(scene)){
            const scenesNames = Object.getOwnPropertyNames(this.scenes);
            if (scenesNames.length > scene){
                sceneName = scenesNames[scene];
            }
        } 
        else if (isString(scene) && this.scenes.hasOwnProperty(scene)) {
            sceneName = scene;
        } 

        if(sceneName){ //scene name successfully defined

            //dispose current scene
            if(this.currentScene && this.currentScene instanceof Scene){
                this.currentScene.dispose();
                delete this.currentScene;
            }
            
            //show next scene
            this.currentScene = new this.scenes[sceneName]({
                game: this.game,
                renderer: this.renderer,
                canvas: this.canvas,
                physicsStats: this.physicsStats,
                loader: this.loader,
                preloader: this.preloader.mesh,
                name: sceneName,
                isDebug: this.isDebug
            });
            
            await this.currentScene.init();
            if (this.isDebug) {
                //injection for ThreeJS Chrome inspector
                window.scene = this.currentScene.threeScene;
            }
        }
        else{
            throw new Error(`No such scene found in ScenesManager: ${scene}`);
        } 
    }

    update(delta) {
        this.currentScene.update(delta);
        // requesting spinner animation
        this.preloader.animate();
    }

    resize(width, height) {
        if (this.currentScene) {
            this.currentScene.resize(width, height);
        }
    }
}