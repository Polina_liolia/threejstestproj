import {
    TweenLite
} from '../../node_modules/gsap/TweenLite';
import {
    Texture
} from 'three';
import settings from '../settings';

/*
Super class for other game scenes
Constructor takes object with such props:
renderer - common renderer for all scenes (required)
canvas - single canvas element for all scenes (required)
loader - MyLoader instance (required)
preloader - object to visualize loading process (null is default)
name - name of the scene ("newScene" is default)
isDebug - game mode (false is default)

*/
export default class Scene {
    constructor(params) {

        this.game = params.game;
        this.renderer = params.renderer;
        this.canvas = params.canvas;
        this.physicsStats = params.physicsStats;
        this.loader = params.loader;
        this.preloader = params.preloader || null;
        this.name = params.name || "newScene";
        this.isDebug = params.isDebug || false;
        this.position = {
            x: 0,
            y: 0,
            z: 0
        };

        //all objects that have to be disposed on scene closing
        this.disposable = [];
        this.sceneSubjects = [];

        this.camera = null;
        this.threeScene = null;
        this.ground = null;
        this.audioSource = null;

        this.lights = {
            hemisphereLight: null,
            directionalLight: null,
            pointLight: null,
            ambientLight: null,
            rectLight: null,
            spotLight: null
        };

        this.debug = {
            physicalBody1: null,
            physicalBody2: null
        };
    }

    async init() {
        this.createScene(false);
        this.setUpCamera(this.isDebug);
        this.setUpAudio();

        this.showPreloader();
        const loadSt = (new Date()).getTime();
        await this.loadResources();
        const loadEnd = (new Date()).getTime();
        console.log(`[Load time]: ${(loadEnd - loadSt) / 1000} s`);
        this.showPreloader(false);
        this.threeScene.background = new THREE.Color(0xa0a0a0);

        this.enablePhysics(); // run physics
        this.setUpPhysics(); //custom settings for physics
        this.setUpLight();
        this.createGround(this.isDebug);
        this.createSceneSubjects();

        console.log("scene created");
    }

    //has to be implemented in derived classes
    async loadResources() {

    }

    showPreloader(isVisible = true) {
        if (this.preloader) {
            this.preloader.visible = isVisible;
        }
    }

    
    //has to be implemented in derived classes
    createSceneSubjects() {

    }

    enablePhysics() {
        Physijs.scripts.worker = './physijs_worker.js';
        Physijs.scripts.ammo = './ammo.js';
        
        this.threeScene.addEventListener(
			'update',
			() => {
				this.threeScene.simulate(undefined, 2); // run physics
				this.physicsStats.update();
			}
        );
        this.threeScene.simulate();
        
    }

    //override this to add custom settings for physics on scene
    setUpPhysics() {

    }

    //can be overridden in derived classes
    setUpCamera(hasOrbitControls = false) {
        const height = settings.height; //window.innerHeight;
        const width = settings.width; //height / 0.46;
        this.camera = new THREE.PerspectiveCamera(50, //field of view
            // window.innerWidth / window.innerHeight, //aspect ratio
            width / height, //aspect ratio
            1, //near clipping plane
            3000); //far clipping plane
        this.camera.name = "PerspectiveCamera";
        this.camera.position.set(0, 550, 1800);
        this.camera.lookAt(0, 0, 0);
        if (hasOrbitControls) {
            // const controls = new THREE.OrbitControls(this.camera);
            // controls.target.set(0, 100, 0);
            // controls.update();
            // this.disposable.push(controls);
        }

    }

    setUpAudio() {
        const listener = new THREE.AudioListener();
        this.camera.add(listener);

        // create a global audio source
        this.audioSource = new THREE.Audio(listener);
    }

    createScene(hasFog) {
        this.threeScene = new Physijs.Scene({ reportsize: 50, fixedTimeStep: 1 / 80 });
        this.threeScene.name = this.name;
        this.threeScene.background = new THREE.Color(0x000000);
        if (hasFog)
            this.threeScene.fog = new THREE.Fog(0xa0a0a0, 200, 1000);
        if (this.preloader)
            this.threeScene.add(this.preloader);
    }

    //creates hemisphereLight and directionalLight by default
    //can be overridden or extended in derived classes
    setUpLight() {
        this.lights.hemisphereLight = new THREE.HemisphereLight(0xffffff, // The light being visualized. 
            0x444444 //The size of the mesh used to visualize the light.
        );
        this.lights.hemisphereLight.position.set(0, 200, 0);
        this.lights.hemisphereLight.name = "HemisphereLight";
        this.threeScene.add(this.lights.hemisphereLight);

        this.lights.directionalLight = new THREE.DirectionalLight(0xffffff, //color
            1); //intensity
        this.lights.directionalLight.name = "DirectionalLight";
        this.lights.directionalLight.position.set(0, 200, 100);
        this.lights.directionalLight.castShadow = true;
        this.lights.directionalLight.shadow.camera.top = 180;
        this.lights.directionalLight.shadow.camera.bottom = -100;
        this.lights.directionalLight.shadow.camera.left = -120;
        this.lights.directionalLight.shadow.camera.right = 120;
        this.lights.directionalLight.shadowMapWidth = this.lights.directionalLight.shadowMapHeight = 2048;
        this.lights.directionalLight.shadowDarkness = 0.7;
        this.threeScene.add(this.lights.directionalLight);
    }

    //to override in derived class
    createGround(hasGreed = false) {
        // ground

        const material = Physijs.createMaterial(
            new THREE.MeshPhongMaterial({
                color: 0x999999,
                depthWrite: false
            }),
            0.4,
            0.8
        );
        const mass = 0; //ground is a static body
        this.ground = new Physijs.Mesh(new THREE.PlaneBufferGeometry(2000, 2000), material, mass);
        this.ground.name = "ground";
        this.ground.rotation.x = -Math.PI / 2;
        this.ground.receiveShadow = true;
        this.disposable.push(this.ground); //add to disposable
        this.threeScene.add(this.ground);
        if (hasGreed) {
            // const grid = new THREE.GridHelper(2000, 20, 0x000000, 0x000000);
            // grid.material.opacity = 0.2;
            // grid.material.transparent = true;
            // this.disposable.push(grid); //add to disposable
            // this.threeScene.add(grid);
        }
    }

    setBackground(bg) {

        if (bg instanceof Number) {
            this.threeScene.background = new THREE.Color(bg);
        } else {
            bg.wrapS = THREE.RepeatWrapping;
            bg.wrapT = THREE.RepeatWrapping;
            bg.repeat.y = 1; //-1;
            this.threeScene.background = bg;
        }
    }

    changeGroundTexture(texture, wrapS = THREE.ClampToEdgeWrapping, wrapT =
        THREE.ClampToEdgeWrapping, repeatX = 1, repeatY = 1) {

        texture.wrapS = wrapS;
        texture.wrapT = wrapT;
        texture.repeat.set(repeatX, repeatY);

        // use the texture for material creation
        const material = new THREE.MeshBasicMaterial({
            map: texture,
        });
        this.ground.material.dispose();
        this.ground.material = material;
    }



    update(delta) {
        //this.threeScene.simulate(undefined, 2);
        for (let i = 0; i < this.sceneSubjects.length; i++) {
            if (this.sceneSubjects[i].update)
                this.sceneSubjects[i].update(delta);
        }

        if (this.threeScene && this.camera) {
            const canvas = this.renderer.domElement;
            this.camera.aspect = canvas.clientWidth / canvas.clientHeight;
            this.camera.updateProjectionMatrix();
            this.renderer.render(this.threeScene, this.camera);

        }

    }

    resize(width, height) {
        if (this.camera) {
            this.renderer.setSize(width, height);
            this.camera.aspect = width / height; // canvas.clientWidth / canvas.clientHeight; //width / height;
            this.camera.updateProjectionMatrix();
            //this.canvas.width = width;
            //this.canvas.height = height;
        }
    }

    //clean up memory when scene is not in use any more
    dispose() {

        for (let i = 0; i < this.disposable.length; i++) {
            this._disposeObject(this.disposable[i]);
        }

        for (let i = 0; i < this.sceneSubjects.length; i++) {
            this._disposeObject(this.sceneSubjects[i]);
        }

        this.loader.disposeSceneResources(this.name);
    }

    _disposeObject(object) {
        if (object.dispose) {
            object.dispose();
        } else {
            if (object.geometry)
                object.geometry.dispose();
            if (object.material)
                object.material.dispose();
        }
    }

    //takes string with the name of preloaded sound resource
    playSound(soundResource, loop = false, overlap = false) {
        const clip = this.loader.getResource(this.name, soundResource)
        if (!overlap && this.audioSource.isPlaying) {
            this.audioSource.stop();
        }
        this.audioSource.setBuffer(clip);
        this.audioSource.setLoop(loop);
        this.audioSource.setVolume(0.5);
        this.audioSource.play();
    }


    //managing position:
    get x() {
        return this.position.x;
    }

    set x(value) {
        this.position.x = value;
        this.threeScene.position.x = this.position.x;
    }

    get y() {
        return this.position.y;
    }

    set y(value) {
        this.position.y = value;
        this.threeScene.position.y = this.position.y;
    }

    get z() {
        return this.position.z;
    }

    set z(value) {
        this.position.z = value;
        this.threeScene.position.z = this.position.z;
    }

}