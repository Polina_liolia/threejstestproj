import Scene from "../superClasses/scene";
import {
    TweenLite,
    TimelineLite
} from '../../node_modules/gsap/TweenLite';


/*
Super class for all scene subjects. 
Constructor takes object with props:
name - object's name (optional, default is "sceneSubject")
position - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
rotation - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
size - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
scale - optional, default is 1
color - optional, default is black (0xffffff)
material - THREE material type, optional, default is null
opacity (optional, 1 is default)
resource - preloaded resource for this subject (default is null)
textureWrap - object with params wrapS, wrapT, repeatX, repeatY
hasShadows - boolean, default is true
(optional, default is {wrapS:  THREE.ClampToEdgeWrapping, wrapT:  THREE.ClampToEdgeWrapping, repeatX: 1, repeatY: 1 })
*/

export default class SceneSubject {
    constructor(params) {
        this.name = params.name || "sceneSubject";
        this.position = params.position || {
            x: 0,
            y: 0,
            z: 0
        };

        this.rotation = params.rotation || {
            x: 0,
            y: 0,
            z: 0
        };

        this.size = params.size || {
            x: 0,
            y: 0,
            z: 0
        };
        this.color = params.color || 0xffffff;
        this.hasShadows = params.hasShadows !== null && params.hasShadows !== undefined ? params.hasShadows : true;
        this.hasPhysics = params.hasPhysics !== null && params.hasPhysics !== undefined ? params.hasPhysics : false;
        this.physicsParams = params.physicsParams || {
            mass: 0,
            friction: 0,
            restitution: 0
        };
        this.material = params.material || null;
        this.opacity = params.opacity !== null && params.opacity !== undefined ? params.opacity : 1;
        this.resource = params.resource || null;
        this.textureWrap = params.textureWrap || {
            wrapS: THREE.ClampToEdgeWrapping,
            wrapT: THREE.ClampToEdgeWrapping,
            repeatX: 1,
            repeatY: 1
        };
        this.updatableMethods = [];
        this.init(params);
        this.element = this.createElement();
        this.element.castShadow = this.hasShadows;
        this.element.receiveShadow = this.hasShadows;

        this.scale = params.scale || 1;
        this.setPositionRotation();

        // this.group.position.set(this.position.x, this.position.y, this.position.z);
        //this.group.rotation.set(this.rotation.x, this.rotation.y, this.rotation.z);
        // this.element.scale.set( this.size.x, this.size.y, this.size.z );
    }

    //initializes subject with custom properties
    init(params) {

    }


    //has to be implemented in derived class
    //returns created element 
    createElement() {

    }

    createMaterial() {
        let texture = null;
        if (this.resource && this.resource.isTexture) {
            texture = this.resource;
            texture.wrapS = this.textureWrap.wrapS;
            texture.wrapT = this.textureWrap.wrapT;
            texture.repeat.set(this.textureWrap.repeatX, this.textureWrap.repeatY);
        }

        const materialObject = this.material ? this.material : this.hasShadows ? THREE.MeshLambertMaterial : THREE.MeshBasicMaterial;
 
        // use the texture for material creation
        let material = texture ? new materialObject({
            map: texture,
        }) : new materialObject({
            color: this.color,
            opacity: this.opacity,
        });
        if (this.opacity === 0) {
            material.transparent = true;
        }
        if (this.hasPhysics) {
            const friction = this.physicsParams.friction || 0;
            const restitution = this.physicsParams.restitution || 0;
            material = this.makeMaterialPhysical(material, friction, restitution);
        }
        return material;
    }

    //takes friction and restitution values (float numbers from 0 to 1)
    makeMaterialPhysical(baseMaterial, friction, restitution) {
        return Physijs.createMaterial(
            baseMaterial,
            friction,
            restitution
        );

    }

    addLines(mesh) {
        const edges = new THREE.EdgesGeometry(mesh.geometry);
        const line = new THREE.LineSegments(edges, new THREE.LineBasicMaterial({
            color: this.linesColor
        }));
        mesh.add(line);
    }

    //update all animatable objects
    update(delta) {
        for (let i = 0; i < this.updatableMethods.length; i++) {
            this.updatableMethods[i](delta);
        }
    }

    //takes function, that have deltaTime as a parameter
    onUpdate(func) {
        this.updatableMethods.push(func);
    }

    //takes function, that have deltaTime as a parameter
    offUpdate(func) {
        let index = -1;
        for (let i = 0; i < this.updatableMethods.length; i++) {
            if (this.updatableMethods[i].name === func.name) {
                index = i;
                break;
            }
        }

        if (index >= 0) {
            this.updatableMethods.splice(index, 1);
        } else {
            console.log("[offUpdate]: callback not found");
        }

    }

    //dispose all disposable elements of the subject
    dispose() {
        if (this.element instanceof THREE.Group) {
            for (let i = 0; i < this.element.children.length; i++) {
                this._disposeObject(this.element[i]);
            }
        } else {
            this._disposeObject(this.element);
        }
    }

    _disposeObject(object) {
        if (object.dispose) {
            object.dispose();
        } else {
            if (object.geometry)
                object.geometry.dispose();
            if (object.material)
                object.material.dispose();
        }
    }

    addToStage(stage) {
        if (stage instanceof Scene) {
            stage.threeScene.add(this.element);
            stage.sceneSubjects.push(this);
        }
    }

    setPositionRotation() {
        this.x = this.position.x;
        this.y = this.position.y;
        this.z = this.position.z;
        this.rotationX = this.rotation.x;
        this.rotationY = this.rotation.y;
        this.rotationZ = this.rotation.z;

    }

    //managing position:
    get x() {
        return this.position.x;
    }

    set x(value) {
        this.position.x = value;
        this.element.position.x = this.position.x;
        this.element.__dirtyPosition = true;
    }

    get y() {
        return this.position.y;
    }

    set y(value) {
        this.position.y = value;
        this.element.position.y = this.position.y;
        this.element.__dirtyPosition = true;
    }

    get z() {
        return this.position.z;
    }

    set z(value) {
        this.position.z = value;
        this.element.position.z = this.position.z;
        this.element.__dirtyPosition = true;
    }

    //managing rotation:
    get rotationX() {
        return this.rotation.x;
    }

    set rotationX(value) {
        this.rotation.x = value;
        this.element.rotation.x = this.rotation.x;
        this.element.__dirtyRotation = true;
    }

    get rotationY() {
        return this.rotation.y;
    }

    set rotationY(value) {
        this.rotation.y = value;
        this.element.rotation.y = this.rotation.y;
        this.element.__dirtyRotation = true;
    }

    get rotationZ() {
        return this.rotation.z;
    }

    set rotationZ(value) {
        this.rotation.z = value;
        this.element.rotation.z = this.rotation.z;
        this.element.__dirtyRotation = true;
    }

    //managing size:
    get width() {
        return this.size.x;
    }

    set width(value) {
        this.size.x = value;
        this.element.scale.x = this.size.x;
    }

    get height() {
        return this.size.y;
    }

    set height(value) {
        this.size.y = value;
        this.element.scale.y = this.size.y;
    }

    get depth() {
        return this.size.z;
    }

    set depth(value) {
        this.size.z = value;
        this.element.scale.z = this.size.z;
    }

    //managing scale:
    get scale() {
        return this._scale;
    }

    set scale(value) {
        this._scale = value;
        if (this.element && this.element.scale) {
            this.element.scale.set(value, value, value);
        }
    }

    async moveToPosition(position, duration, onMoveComplete = null) {
        return new Promise(resolve => {
            TweenLite.to(this, duration, {
                x: position.x || this.x,
                y: position.y || this.y,
                z: position.z || this.z,
                onComplete: () => {
                    if(onMoveComplete){
                        onMoveComplete();
                    }
                    resolve();
                    console.log(`${this.name} moving completed`);
                }
            });
        });
    }

    changeTexture(newTexture) {
        this.element.material.map = newTexture;
    }




}