import Scene from "./scene";

export default class UiScene extends Scene{
    constructor(params){
        super(params);
        this.initUI();
        this.eventsListeners = []; //all html elements, having event listeners, should be added here
        this.setUpUI();
    }

    //displays html div for current scene
    initUI(){
        this.container = $(`#${this.name}`);
        this.container.width($(this.canvas).width());
        this.container.height($(this.canvas).height());
        this.container.show();
    }

    //custom settings for scene elements
    //all html elements, having event listeners, should be added to this.eventsListeners array
    setUpUI(){
        
    }

    resize(width, height) {
        this.container.width(width);
        this.container.height(height);
    }

    dispose(){
        this.container.hide();
        for (let i = 0; i < this.eventsListeners.length; i++){
            if(this.eventsListeners[i].off){
                this.eventsListeners.off(); //remove all event listeners
            }
        }
    }
}