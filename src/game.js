

export default class Game{
    constructor(){
        this._player = null; //new Player(info, stats, character)
        this._opponent = null;
        this.scenesManager = null;     
    }

    set opponent(value){
        this._opponent = value;
    }

    get opponent(){
        return this._opponent;
    }

    
}