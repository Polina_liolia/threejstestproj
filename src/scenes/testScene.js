import Scene from "../superClasses/scene";
import Model3D from "../elements/model3D";
import Plane from "../elements/plane";
import Box from "../elements/box";
import Sphere from "../elements/sphere";
import {
    cloneFbx
} from "../helpers/functions";

export default class TestScene extends Scene {

    constructor(renderer, canvas, name, isDebug = false) {
        super(renderer, canvas, name, isDebug);

    }

    async loadResources() {

        // this.loader.addFbx(this.name, "high_poly", "./assets/test_poly/6144.FBX");

        this.loader.addGTlf(this.name, "high_poly", "./assets/test_poly/393216.glb");

       // this.loader.addTexture(this.name, "ball_texture", "./assets/ball.jpg");
        //this.loader.addTexture(this.name, "court_clay", "./assets/texture_court_2.png");

        //  this.loader.addFbx(this.name, "boy", "../assets/Tennis_Boy/Tennis_Boy_01.fbx");
        //   this.loader.addTexture(this.name, "cloth_texture", "../assets/Tennis_Boy/Cloth1_Base_Color.png");
        //   this.loader.addTexture(this.name, "cloth_mask", "../assets/Tennis_Boy/Cloth1_element_mask.png");


        await this.loader.loadAll();
    }

    setUpCamera(hasOrbitControls = false) {
        this.camera = new THREE.PerspectiveCamera(30, //field of view
            window.innerWidth / window.innerHeight, //aspect ratio
            1, //near clipping plane
            3000); //far clipping plane
        this.camera.name = "PerspectiveCamera";
        this.camera.position.set(0, 270, 900);
        this.camera.lookAt(0, 0, 0);

        //set up orbit controls
        const controls = new THREE.OrbitControls(this.camera);
        controls.target.set(0, 100, 0);
        controls.update();
        this.disposable.push(controls);
    }

    setUpLight() {
        this.lights.hemisphereLight = new THREE.HemisphereLight(0xffffff, // The light being visualized. 
            0x444444 //The size of the mesh used to visualize the light.
        );
        this.lights.hemisphereLight.position.set(0, 200, 0);
        this.lights.hemisphereLight.name = "HemisphereLight";
        this.threeScene.add(this.lights.hemisphereLight);

        this.lights.directionalLight = new THREE.DirectionalLight(0xffffff, //color
            0.3); //intensity
        this.lights.directionalLight.name = "DirectionalLight";
        this.lights.directionalLight.position.set(200, 200, 0);
        this.lights.directionalLight.castShadow = true;
        this.lights.directionalLight.shadow.camera.top = 200;
        this.lights.directionalLight.shadow.camera.bottom = -200;
        this.lights.directionalLight.shadow.camera.left = -320;
        this.lights.directionalLight.shadow.camera.right = 320;
        this.threeScene.add(this.lights.directionalLight);
    }


    createSceneSubjects() {
        this.ground.material.blending = THREE.MultiplyBlending;

        this.createHighPoly();

      //  this.createCourtSample();

        //  this.createBoy();

        // this.createFrog();

    }

    setUpPhysics() {
        this.threeScene.setGravity(new THREE.Vector3(0, -150, 0));
    }

    createHighPoly(){
        this.highPoly = new Model3D({
            name: "high_poly",
            resource: this.loader.getResource(this.name, "high_poly"),
            position: {
                x: 0,
                y: 0,
                z: 0
            },
            scale: 1,
            isAnimated: false,
        });
        this.highPoly.addToStage(this);
    }

    createCourtSample() {
        this.groundPlane = new Box({
            name: "ground_plane",
            resource: this.loader.getResource(this.name, "court_clay"),
            rotation: {
                x: 0,
                y: 0,
                z: 0
            },
            position: {
                x: 0,
                y: 0,
                z: 0
            },
            hasPhysics: true,
            physicsParams: {
                mass: 0,
                friction: 0.8,
                restitution: 0.8
            },
            // hasShadows: true,
            size: {
                x: 780,
                y: 1,
                z: 1150
            },
        });
        this.groundPlane.addToStage(this);
        this.groundPlane.element.receiveShadow = true;
        this.groundPlane.element.castShadow = false;
        // this.groundPlane.element.layers.set(2);

        this.ball = new Sphere({
            name: "ball",
            radius: 2.5,
            widthSegments: 100,
            heightSegments: 100,
            position: {
                x: 40,
                y: 100,
                z: 400
            },
            resource: this.loader.getResource(this.name, "ball_texture"),
            hasPhysics: true,
            physicsParams: {
                mass: 20,
                friction: 0.8,
                restitution: 1
            }
        });
        //this.ball.element.setDamping(0, 0.9);
        

       
        
        this.ball.addToStage(this);

        this.ball.element.setLinearVelocity(new THREE.Vector3(0, -80, -600));
      //  this.ball.element.setAngularVelocity(new THREE.Vector3(0, 155, 0));

        //this.ball.element.setAngularFactor(new THREE.Vector3(0, 55, 0));
        //this.ball.element.setLinearFactor(new THREE.Vector3(0, 55, 0));
    }

    createBoy() {
        this.boy = new Model3D({
            name: "boy",
            resource: cloneFbx(this.loader.getResource(this.name, "boy")),
            position: {
                x: 0,
                y: 0,
                z: 240
            },
            scale: 1,
            isAnimated: false,

        });

        var texture = this.loader.getResource(this.name, "cloth_texture");
        texture.needsUpdate = true; // important
        var mask = this.loader.getResource(this.name, "cloth_mask");
        mask.needsUpdate = true; // important

        // uniforms
        var uniforms = {
            shorts_color: {
                type: "c",
                value: new THREE.Color(0x306ecc)
            }, // mask is "green"
            tshirt_color: {
                type: "c",
                value: new THREE.Color(0x30cc7d)
            }, // mask is "red"
            base_texture: {
                type: "t",
                value: texture
            },
            color_mask: {
                type: "t",
                value: mask
            },
        };

        // attributes
        var attributes = {};

        // material
        var material = new THREE.ShaderMaterial({
            uniforms: uniforms,
            vertexShader: document.getElementById('vertex_shader').textContent,
            fragmentShader: document.getElementById('fragment_shader').textContent
        });


        this.boy.element.children[3].material = material;
        this.boy.addToStage(this);


    }

    createFrog() {
        this.frog = new Model3D({
            name: "frog",
            resource: this.loader.getResource(this.name, "frog"),
            position: {
                x: 200,
                y: 0,
                z: 200
            },
            scale: 30,
            isAnimated: false
        });
        this.frog.addToStage(this);
        setTimeout(() => {
            this.frog.setShape("ncl1175.PaskalAge01_eating2Shape", 1);
        }, 2000);
    }

    update(delta) {
        super.update(delta);
    }

}