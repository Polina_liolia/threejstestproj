export const PLAYER = 0,
    OPPONENT = 1;

export const gameStates = Object.freeze({
    preGame: 1,
    started: 2,
    completed: 3
});



export default class TennisController {
    constructor(photonServer, player, opponent, ball) {
        this.photonServer = photonServer;
        this.player = player;
        this.opponent = opponent;
        this._ball = ball;

        //defaults:
        this._state = gameStates.preGame;
        this._server = this.photonServer.isGameOrganizer ? PLAYER : OPPONENT;
        this._currentTurn = this._server; //whose turn is now

        this.registerActions();

    }

    get server() {
        return this._server;
    }

    set server(val) {
        this._server = val;
        if (this._state === gameStates.preGame) {
            this._currentTurn = this._server;
        }
    }

    get ball(){
        return this._ball;
    }

    set ball(val){
        this._ball = val;
        if(!this.photonServer.isGameOrganizer){
            this._ball.z *= -1; //if player is not server - move ball to opponent
        }
    }

    registerActions() {
        const $canvas = $("body>div>canvas");
        $canvas.tap(function () {
            if (this._state === gameStates.preGame) {
                this.ball.throwUp();
                this._state = gameStates.started;
            }
        }.bind(this));

        $canvas.swipeup(function (e) {
            if (this._currentTurn === PLAYER) {
                this.photonServer.sendMessage("beat!");
                const randomSign = Math.random() < 0.5 ? -1 : 1;
                this.ball.throwTo(new THREE.Vector3(100 * randomSign, 3, -200));
                this._currentTurn = OPPONENT;
            }

        }.bind(this));

        //_____change to mutliplayer callback_____
        this.photonServer.onEvent = (code, content, actorNr) => {
            if (this._currentTurn === OPPONENT) {
                this.photonServer.output(`message: ${content.message}`);
                const randomSign = Math.random() < 0.5 ? -1 : 1;
                this.ball.throwTo(new THREE.Vector3(100 * randomSign, 3, 200));
                this._currentTurn = PLAYER;
            }

        };
    }
}