import SceneSubject from "../superClasses/sceneSubject";

/*Constructor takes object with props:

-inherited:
name - object's name (optional, default is "sceneSubject")
position - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
rotation - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
size - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
resource - preloaded resource for this subject (default is null)
textureWrap - object with params wrapS, wrapT, repeatX, repeatY
(optional, default is {wrapS:  THREE.ClampToEdgeWrapping, wrapT:  THREE.ClampToEdgeWrapping, repeatX: 1, repeatY: 1 })

-own:
hasPhysics - boolean, default is false
*/

export default class Plane extends SceneSubject {
    constructor(props) {
        super(props);
    }

    //initializes subject with custom properties
    init(params) {
        this.hasPhysics = params.hasPhysics || false;
    }

    //returns created element 
    createElement() {
        const material = this.createMaterial();
        const geometry = new THREE.PlaneBufferGeometry(this.width, this.height);
        geometry.computeFaceNormals();
        geometry.computeVertexNormals();
        const mesh = this.hasPhysics ? Physijs.Mesh : THREE.Mesh;
        const mass = this.physicsParams ? this.physicsParams.mass : 0;
        const plane = new mesh(geometry, material, mass);
        plane.receiveShadow = true;
        plane.name = `${this.name}_plane`;
        if (this.hasLines) {
            this.addLines(sphere);
        }
        return plane;
    }

    //update all animatable objects
    update(delta) {

    }

}