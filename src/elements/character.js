import Model3D from "./model3D";

/*personal properties:
textures - object with preloaded textures for separate character's parts
{body: bodyTexture, cloth: clothTexture, eyes: eyesTexture, hair: hairTexture, shoes: shoesTexture }

masks - object with preloaded masks for separate character's parts
{body: bodyMask, cloth: clothMask, eyes: eyesMask, hair: hairMask, shoes: shoesMask }
*/

export default class Character extends Model3D{
    constructor(options){
        super(options);
    }

    init(options) {
        super.init(options);
        this.textures = options.textures;
        this.masks = options.masks;
    }

    setClothesColor(shortsColor, tshirtColor){
        const texture = this.textures.cloth; 
        texture.needsUpdate = true; // important
        const mask = this.masks.cloth;
        mask.needsUpdate = true; // important

        // uniforms
        var uniforms = {
            shorts_color: {
                type: "c",
                value: new THREE.Color(shortsColor)
            }, // mask is "green"
            tshirt_color: {
                type: "c",
                value: new THREE.Color(tshirtColor)
            }, // mask is "red"
            base_texture: {
                type: "t",
                value: texture
            },
            color_mask:{
                type: "t",
                value: mask
            },
        };

        // attributes
        var attributes = {};

        // material
        var material = new THREE.ShaderMaterial({
            uniforms: uniforms,
            vertexShader: document.getElementById('vertex_shader').textContent,
            fragmentShader: document.getElementById('fragment_cloth_shader').textContent
        });

        this.element.children[3].material = material;
    }

    addPhysics(){
        const material = this.createMaterial();
        material.transparent = true;
        material.opacity = 0;
        material.depthTest = false;

        const boxSize = this.defineSize();

        const radius = Math.max(boxSize.x, boxSize.z) / 2;
        this.physicsContainer = new Physijs.CapsuleMesh(
            new THREE.CylinderGeometry( radius , radius, boxSize.y, 8, 1 ),
            material,
            this.physicsParams.mass
        );
        this.physicsContainer.name = `${this.name}_physicsBody`;
        this.physicsContainer.position.set(this.x, this.y, this.z);
        this.physicsContainer.add(this.element);
        this.element.position.set(radius / 2, 0, radius / 2);
           
    }
}