import SceneSubject from "../superClasses/sceneSubject";

/*
Constructor takes object with props:

-inherited:
name - object's name (optional, default is "sceneSubject")
position - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
rotation - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
size - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
resource - preloaded resource for this subject (default is null)
textureWrap - object with params wrapS, wrapT, repeatX, repeatY
(optional, default is {wrapS:  THREE.ClampToEdgeWrapping, wrapT:  THREE.ClampToEdgeWrapping, repeatX: 1, repeatY: 1 })
-own:
radius - sphere radius. Default is 1.
widthSegments - number of horizontal segments. Minimum value is 3, and the default is 8.
heightSegments - number of vertical segments. Minimum value is 2, and the default is 6.
*/
export default class Sphere extends SceneSubject {
    constructor(params) {
        super(params);
    }

    //initializes subject with custom properties
    init(params) {
        this.radius = params.radius || 1;
        this.widthSegments = params.widthSegments || 8;
        this.heightSegments = params.heightSegments || 6;
    }

    createElement() {
        const geometry = new THREE.SphereBufferGeometry(this.radius, this.widthSegments, this.heightSegments);
        const material = this.createMaterial();
        const mesh = this.hasPhysics ? Physijs.SphereMesh : THREE.Mesh;
        const mass = this.physicsParams ? this.physicsParams.mass : 0;
        const sphere = new mesh(geometry, material, mass);
        sphere.name = `${this.name}_sphere`;

        if (this.hasLines) {
            this.addLines(sphere);
        }
        return sphere;
    }
}