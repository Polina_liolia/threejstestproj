import Sphere from "../elements/sphere";
import {
    degreesToRadians
} from "../helpers/functions";

export default class Ball extends Sphere {
    constructor(params) {
        super(params);
        this.prevPosition = this.element.position;

        if (this.hasPhysics) {
            //Enable CCD if the object moves more than 1 meter in one simulation frame
            this.element.setCcdMotionThreshold(1);
            // Set the radius of the embedded sphere such that it is smaller than the object
            this.element.setCcdSweptSphereRadius(0.2);
        }


        //for debug:
        this.element.addEventListener('collision', (otherObject, relativeVelocity, relativeRotation, contactNormal) => {
            // `this` has collided with `other_object` with an impact speed of `relative_velocity` 
            //and a rotational force of `relative_rotation` and at normal `contact_normal`
            const position = this.element.position.floor();
            relativeVelocity = relativeVelocity.floor();
            relativeRotation = relativeRotation.floor();
            contactNormal = contactNormal.floor();

            console.log(`ball collision with ${otherObject.name} 
            in position ${position.x}, ${position.y}, ${position.z}
            relative velocity: ${relativeVelocity.x}, ${relativeVelocity.y}, ${relativeVelocity.z}
            relative rotation: ${relativeRotation.x}, ${relativeRotation.y}, ${relativeRotation.z}
            contact normal: ${contactNormal.x}, ${contactNormal.y}, ${contactNormal.z}`);
        });
    }

    resetVelocity() {
        this.element.setLinearVelocity(new THREE.Vector3(0, 0, 0));
        this.element.setAngularVelocity(new THREE.Vector3(0, 0, 0));


    }

    resetEnergy() {
        this.element.setLinearFactor(new THREE.Vector3(1, 1, 1));
        this.element.setAngularFactor(new THREE.Vector3(1, 1, 1));
    }

    throwUp(y = 50) {
        if (this.hasPhysics) {
            this.resetVelocity();
        }
        this.moveToPosition(new THREE.Vector3(null, y, null), 0.5);
        //this.element.setLinearVelocity(new THREE.Vector3(0, y, 0));
    }

    //takes Vector3 with coordinates of target position
    throwTo(targetPos, degrees = 30) {
        this.prevPosition = this.element.position;
        if (this.hasPhysics) {
            this.resetVelocity();
        }
       // targetPos = new THREE.Vector3(100, 3, 200);
        this.moveToPosition(targetPos, 1, this.onCollision.bind(this));
       
    }

    onCollision() {
        if (this.hasPhysics) {
            this.resetVelocity();
        }

        const sign = this.z < 0 ? -1 : 1;
        const currentPosition  =  this.element.position;
        const deltaY = 500;
        const targetX = deltaY * ((currentPosition.x - this.prevPosition.x) / Math.abs(currentPosition.z - this.prevPosition.z));
        const targetPos = new THREE.Vector3(targetX, this.prevPosition.y * 0.8, this.z + sign * deltaY);
        this.moveToPosition(targetPos, 3);
    }

    throwWithPhysics(targetPos, degrees) {

        this.resetVelocity();
        const startPos = this.element.position;
        const distance = startPos.distanceTo(targetPos);
        const direction = targetPos.sub(startPos).normalize();
        direction.y = degrees / 90; //0.5;// degreesToRadians(degrees); //0.5 for 45 degrees
        const force = direction.multiplyScalar(this.element.mass).multiplyScalar(Math.sqrt(distance * 9.81));

        //   this.element.setLinearFactor(new THREE.Vector3(1.5,1.5,1.5));
        this.element.applyImpulse(force, new THREE.Vector3(0, 1, this.radius / 2));
    }

    

}