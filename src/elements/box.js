import SceneSubject from "../superClasses/sceneSubject";

/*Constructor takes object with props:

-inherited:
name - object's name (optional, default is "sceneSubject")
position - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
rotation - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
size - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
resource - preloaded resource for this subject (default is null)
textureWrap - object with params wrapS, wrapT, repeatX, repeatY
(optional, default is {wrapS:  THREE.ClampToEdgeWrapping, wrapT:  THREE.ClampToEdgeWrapping, repeatX: 1, repeatY: 1 })
-own:
color (optional, 0xffffff is default)

hasLines - if object has EdgesGeometry (optional, false is default)
linesColor (optional, 0xffffff is default)
*/

export default class Box extends SceneSubject{
    constructor(params) {
        super(params);
    }

    init(params) {
        this.hasLines = params.hasLines || false;
        this.linesColor = params.linesColor || 0xffffff;
    }

    createElement() {
        const geometry = new THREE.BoxBufferGeometry(this.width, this.height, this.depth);
        geometry.computeFaceNormals();
        geometry.computeVertexNormals();
        const material = this.createMaterial();
        const mesh = this.hasPhysics ? Physijs.BoxMesh : THREE.Mesh;
        const mass = this.physicsParams ? this.physicsParams.mass : 0;
        const cube = new mesh(geometry, material, mass);
        cube.receiveShadow = true;
        cube.name = `${this.name}_cube`;
        
        if(this.hasLines){
            this.addLines(cube);
        }

       
        return cube;
    }

    
}