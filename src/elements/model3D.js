import SceneSubject from "../superClasses/sceneSubject";
import Scene from "../superClasses/scene";
import {
    clone,
    cloneFbx
} from "../helpers/functions";

/*Constructor takes object with props:

-inherited:
name - object's name (optional, default is "sceneSubject")
position - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
rotation - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
size - object with properties x, y, z (optional, default is {x: 0, y: 0, z: 0})
resource - preloaded resource for this subject (default is null)
textureWrap - object with params wrapS, wrapT, repeatX, repeatY
(optional, default is {wrapS:  THREE.ClampToEdgeWrapping, wrapT:  THREE.ClampToEdgeWrapping, repeatX: 1, repeatY: 1 })
-own:
isAnimated - boolean (false is default)
totalFrames - total number of frames (to manage animations, 0 is default)
animations - object with structure {animationName: {startFrame: frameNumber, endFrame: frameNumber }} (optional, null is default)
*/

export default class Model3D extends SceneSubject {
    constructor(params) {
        super(params);
    }

    //initializes subject with custom properties
    init(params) {
        this.isAnimated = params.isAnimated || false;
        this.animations = params.animations || null;
        this.totalFrames = params.totalFrames || 0;
        this.mixer = null;
        this.animationAction = null;
        this._currentAnimation = null; //object with structure {animationName: {startFrame: frameNumber, endFrame: frameNumber }} 
        this._loopAnimation = false;
        this._chainedAnimations = null;
        this._animationSpeed = 1;
    }

    createElement() {
        const model = this.resource;
        model.name = `${this.name}_3Dmodel`;

        if (this.isAnimated && model && model.animations.length > 0) {
            this.mixer = new THREE.AnimationMixer(model);
            this.animationAction = this.mixer.clipAction(model.animations[0]);
            this.animationAction.play();
            if (!this.animations || !this.animations["idle"]) { //default animation to play
                this.animations = this.animations || {};
                this.animations["idle"] = {
                    startSec: 0,
                    endSec: this.animationAction._clip.duration
                };
            }
            this.animations = this._framesToSeconds(this.totalFrames, this.animationAction._clip.duration, this.animations);
            this.setAnimation("idle", true);
        }

        model.traverse(function (child) {
            if (child.isMesh) {
                child.castShadow = true;
                child.receiveShadow = true;
            }
        });
        return model;
    }

    //_______________________Animations management_____________________

    get animationSpeed() {
        return this._animationSpeed;
    }

    //A value of 0 causes the animation to pause. 
    //Negative values cause the animation to play backwards. 
    //Default is 1
    set animationSpeed(value) {
        this._animationSpeed = value;
        if (this.animationAction) {
            this.animationAction.timeScale = value;
        }
    }

    //total frames number, total animationDuration and object with structure {animationName: {startFrame: frameNumber, endFrame: frameNumber }} 
    //returns object with extra props startSec and endSec (frames converted to seconds)
    _framesToSeconds(totalFrames, animationDuration, obj) {
        const allAnimationsData = {};
        for (const animationName in obj) {
            if (obj.hasOwnProperty(animationName)) {
                const animationData = obj[animationName];
                allAnimationsData[animationName] = animationData;
                if (animationData.startFrame !== null && animationData.startFrame !== undefined &&
                    animationData.endFrame !== null && animationData.endFrame !== undefined) {
                    const startSec = animationDuration * (animationData.startFrame / totalFrames);
                    const endSec = animationDuration * (animationData.endFrame / totalFrames);
                    allAnimationsData[animationName].startSec = startSec;
                    allAnimationsData[animationName].endSec = endSec;
                }
            }
        }
        return allAnimationsData;
    }

    //sets and runs one animation from this.animations object
    setAnimation(name, isLoop = false) {
        if (this.isAnimated && this.animationAction && this.animations && this.animations[name]) { //default animation to play
            this._currentAnimation = this.animations[name];
            this._currentAnimation.name = name;
            this.animationAction.time = this._currentAnimation.startSec;
            this._loopAnimation = isLoop;
            console.log(`current animation: ${name}`);
        }
    }

    //takes array of animations to chain and boolean value if whole chain has to be looped
    setAnimationsChain(animationsNames, isLoop) {
        this._chainedAnimations = animationsNames;
        this.isLoop = isLoop;
    }

    //takes animation name and position index to insert it (inserts to end by default)
    addToAnimationsChain(name, positionIndex = -1) {
        if (positionIndex < 0 || !this._chainedAnimations ||
            this._chainedAnimations.length === 0 ||
            this._chainedAnimations.length - 1 < positionIndex) {
            this._chainedAnimations = this._chainedAnimations || [];
        } else {
            this._chainedAnimations.splice(positionIndex, 0, name);
        }
    }

    //updatable method to control animations part in single clip
    _controlAnimation() {
        if (this._currentAnimation && this.animationAction) {
            if (this.animationAction.time >= this._currentAnimation.endSec ||
                this.animationAction.time < this._currentAnimation.startSec) {
                //for chained animations
                if (this._chainedAnimations && this._chainedAnimations.length > 0) {
                    const currentIndex = this._chainedAnimations.indexOf(this._currentAnimation.name);
                    if (currentIndex > -1) {
                        if (this._chainedAnimations.length > currentIndex + 1) {
                            this.setAnimation(this._chainedAnimations[currentIndex + 1], false);
                        } else {
                            if (this.isLoop) { //whole chain looped
                                this.setAnimation(this._chainedAnimations[0], false);
                            } else { //the last animation in chain will be looped
                                this.setAnimation(this._chainedAnimations[currentIndex], true);
                                this._chainedAnimations = null; //stop chain
                            }
                        }
                    }
                } else if (this._loopAnimation) {
                    this.animationAction.time = this._currentAnimation.startSec;
                } else {
                    this.setAnimation("idle", true);
                }
            }
        }
    }

    //update all animatable objects
    update(delta) {
        super.update();
        if (this.mixer) {
            this.mixer.update(delta);
        }
        this._controlAnimation();
    }

    //____________________Blend shapes management_______________________
    //returns true if current fbx model has any bland shapes or false if not
    _hasBlendShapes() {
        let hasBlendShapes = false;
        if (this.element.children &&
            this.element.children.length > 0 &&
            this.element.children[0].morphTargetInfluences &&
            this.element.children[0].morphTargetInfluences.length > 0) {

            hasBlendShapes = true;
        }
        return hasBlendShapes;
    }

    //returns index of blend shape or -1 if not found
    _getShapeIndex(shapeName) {
        let index = -1;
        if (this._hasBlendShapes() &&
            this.element.children[0].morphTargetDictionary.hasOwnProperty(shapeName)) {

            index = this.element.children[0].morphTargetDictionary[shapeName];
        }
        return index;
    }

    //sets blend shape's value 
    //takes blend shape's name and it's new value
    setShape(name, value) {
        const shapeIndex = this._getShapeIndex(name);
        if (shapeIndex > -1) {
            this.element.children[0].morphTargetInfluences[shapeIndex] = value;
        } else {
            console.log(`[blend shape] ${name} not found in ${this.name}`);
        }
    }

    //____________________Skin colors management_______________________

    //takes name or index of mesh and new color in HEX
    setColor(meshIdent, newColor) {
        //searching for mesh:

        let mesh = null;
        for (let i = 0; i < this.element.children.length; i++) {
            const node = this.element.children[i];
            if (node.type === "SkinnedMesh" && (node.name === meshIdent || i === meshIdent)) {
                mesh = node;
                break;
            }
        }
        if (mesh) {
            mesh.material.emissive.setHex(newColor);
        } else {
            console.log(`[set color] failed: mesh ${meshIdent} not found`);
        }
    }

    defineSize(){
        const size = {
            x: 0,
            y: 0,
            z: 0
        };

        let xMin = 0, xMax = 0,
        yMin = 0, yMax = 0,
        zMin = 0, zMax = 0;
        const childrenNumber = this.element.children.length;
        for(let i = 0; i < childrenNumber; i++){
            const child = this.element.children[i];
            if(child.isMesh){
                child.geometry.computeBoundingBox();
                const boundingBox = child.geometry.boundingBox;
                xMax = Math.max(xMax, boundingBox.max.x);
                xMin = Math.min(xMin, boundingBox.min.x);
                yMax = Math.max(yMax, boundingBox.max.y);
                yMin = Math.min(yMin, boundingBox.min.y);
                zMax = Math.max(zMax, boundingBox.max.z);
                zMin = Math.min(zMin, boundingBox.min.z);
               // console.log(boundingBox);
            }
        }
        size.x = (xMax - xMin) * this.scale;
        size.y = (yMax - yMin) * this.scale;
        size.z = (zMax - zMin) * this.scale;
        return size;
    }

    addPhysics(){
        const material = this.createMaterial();
        material.transparent = true;
        material.opacity = 0;
        material.depthTest = false;

        const boxSize = this.defineSize();

        const geometryObject = this.physicsParams.basicGeometry || THREE.CubeGeometry;
        const physicsGeometryObject = this.physicsParams.physicGeometry || Physijs.BoxMesh;

        this.physicsContainer = new physicsGeometryObject(
            new geometryObject( boxSize.x + 2 , boxSize.y + 2, boxSize.z + 2 ),
            material,
            this.physicsParams.mass
        );
        this.physicsContainer.name = `${this.name}_physicsBody`;
        this.physicsContainer.position.set(this.x, this.y, this.z);
        this.physicsContainer.add(this.element);
        this.element.position.set(0, 0, 0);
           
    }

    addToStage(stage) {
        if(this.hasPhysics){
            this.addPhysics();
        }
        if (stage instanceof Scene) {
            const child = this.physicsContainer || this.element;
            stage.threeScene.add(child);
            stage.sceneSubjects.push(this);
        }
    }
}