import ScenesManager from "./scenesManager";
import settings from "./settings";
import Game from "./game";

export const Renderers = Object.freeze({
    WebGl1: 0,
    WebGl2: 1,
    NoWebGl: 2
});

export default class App {

    constructor(isDebug = false) {
        //properties initializing: 
        this.game = null;
        this.clock = new THREE.Clock(); //to define delta on update
        this.renderer = null;
        this.canvas = null;
        this.renderStats = null;
        this.physicsStats = null;

        //check if WebGL is supported and what type of renderer can be used
        const rendererType = this.defineRendererType();

        if (rendererType !== Renderers.NoWebGl) {
            this.injectRequestAnimFrame(); //cross browser animate support

            //create renderer
            const result = this.createRenderer(rendererType);
            this.renderer = result.renderer;
            this.renderer.shadowMapEnabled = true;
            this.renderer.shadowMapSoft = true;
            this.canvas = result.canvas;
            if (this.renderer && this.canvas) { //renderer created successfully
                //processing window resize event
                window.addEventListener('resize', this.onWindowResize.bind(this), false);
                this.game = new Game();
                //create scenesManager to display scenes
                this.scenesManager = new ScenesManager(this.game, this.renderer, this.canvas, this.physicsStats, isDebug);
                //start updating scene every frame
                this.animate();
            } else {
                console.log("create renderer failed");
            }
        } else {
            document.body.appendChild(WEBGL.getWebGLErrorMessage());
            console.log("No WebGL renderer support");
        }
    }

    defineRendererType() {
        let rendererType = Renderers.NoWebGl;
        if (WEBGL.isWebGL2Available()) {
            rendererType = Renderers.WebGl2;
            console.log("WebGL2 renderer");
        } else if (WEBGL.isWebGLAvailable()) {
            rendererType = Renderers.WebGl1;
            console.log("WebGL1 renderer");
        }
        return rendererType;
    }

    createRenderer(rendererType) {
        //setting up renderer
        let renderer = null,
            canvas = null;

        switch (rendererType) {
            case Renderers.WebGl1:
                {
                    renderer = new THREE.WebGLRenderer({
                        antialias: true
                    });
                }
                break;
            case Renderers.WebGl2:
                {
                    canvas = document.createElement('canvas');
                    const context = canvas.getContext('webgl2');
                    renderer = new THREE.WebGLRenderer({
                        canvas: canvas,
                        context: context
                    });
                }
                break;
            default:
                return {
                    renderer,
                    canvas
                };
        }

        renderer.setPixelRatio(window.devicePixelRatio);

        const container = document.createElement('div');
        document.body.appendChild(container);
        canvas = renderer.domElement;
        container.appendChild(canvas);

        this.renderStats = new Stats();
		this.renderStats.domElement.style.position = 'absolute';
        this.renderStats.domElement.style.top = '50px';
		this.renderStats.domElement.style.zIndex = 100;
		container.appendChild( this.renderStats.domElement );
		
		this.physicsStats = new Stats();
		this.physicsStats.domElement.style.position = 'absolute';
        this.physicsStats.domElement.style.top = '100px';
		this.physicsStats.domElement.style.zIndex = 100;
		container.appendChild( this.physicsStats.domElement );
       
        const {
            width,
            height
        } = this.resize(canvas);
        renderer.setSize(width, height);
        renderer.shadowMap.enabled = true;
        //adding renderer to DOM

        return {
            renderer,
            canvas
        };
    }

    //update current scene each frame
    animate() {
        requestAnimFrame(this.animate.bind(this));
        const delta = this.clock.getDelta();
        this.renderStats.update();
       // this.physicsStats.update();

        this.scenesManager.update(delta);
    }

    //process window resize event to make stage fit new window size
    onWindowResize() {
        const {
            width,
            height
        } = this.resize();

        if (this.scenesManager)
            this.scenesManager.resize(width, height);


    }

    resize(canvas = null) {
        let height, width;
        const proportionReal = window.innerHeight / window.innerWidth;
        const proportionBasic = 0.46;

        const dif = Math.abs(proportionReal - proportionBasic);
        //if screen proportions are the same as original
        // if( dif <= 0.05){
        height = window.innerHeight;
        width = height / proportionBasic;
        // }
        // else{
        //     height = window.innerHeight * (1 + dif/10);
        //     width = height / proportionBasic;
        // }

        canvas = canvas || this.canvas;
        if (canvas) {
            const topDif = height - window.innerHeight;
            canvas.style.top = -topDif + "px";

            const leftDif = (width - window.innerWidth) / 2;
            canvas.style.left = -leftDif + "px";
        }

        settings.width = width;
        settings.height = height;
        return {
            width,
            height
        };
    }

    //cross browser solution
    injectRequestAnimFrame() {
        /**
         * Provides requestAnimationFrame in a cross browser way.
         */
        window.requestAnimFrame = (function () {
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function ( /* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
                    return window.setTimeout(callback, 1000 / 60);
                };
        })();

        /**
         * Provides cancelAnimationFrame in a cross browser way.
         */
        window.cancelAnimFrame = (function () {
            return window.cancelAnimationFrame ||
                window.webkitCancelAnimationFrame ||
                window.mozCancelAnimationFrame ||
                window.oCancelAnimationFrame ||
                window.msCancelAnimationFrame ||
                window.clearTimeout;
        })();
    }
}