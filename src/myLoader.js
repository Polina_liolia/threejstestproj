import {
    isArray
} from "./helpers/functions";

export default class MyLoader {
    constructor() {
        //files prepared for loading by types:
        this.fbx = {};
        this.gtlf = {};
        this.audio = {};
        this.textures = {};

        //all loaded resources are stored here:
        this.resources = {};
    }

    //takes scene name (if null, loads as "common" resource, that will not be deleted on scene change)
    //name - key to store resource
    //path - path to file in filesystem that has to be loaded
    addFbx(sceneName, name, path) {
        this.fbx[name] = {
            path: path,
            scene: sceneName
        };
    }

    addGTlf(sceneName, name, path) {
        this.gtlf[name] = {
            path: path,
            scene: sceneName
        };
    }

    addSound(sceneName, name, path) {
        this.audio[name] = {
            path: path,
            scene: sceneName
        };
    }

    addTexture(sceneName, name, path) {
        this.textures[name] = {
            path: path,
            scene: sceneName
        };
    }

    async loadFbx() {
        const fbxNumber = Object.getOwnPropertyNames(this.fbx).length;
        if (fbxNumber > 0) {
            const loader = new THREE.FBXLoader();
            for (const name in this.fbx) {
                if (this.fbx.hasOwnProperty(name)) {
                    const path = this.fbx[name].path;
                    const scene = this.fbx[name].scene ? this.fbx[name].scene : "common";
                    await this._loadSingleFbx(loader, path, scene, name);
                    delete this.fbx[name];
                }
            }
        }
    }

    async _loadSingleFbx(loader, path, scene, name) {
        return new Promise((resolve) => {
            loader.load(path, function (object) {
                object.name = name;
                if (!this.resources[scene]) { //create scene resources object if not exists
                    this.resources[scene] = {};
                }
                this.resources[scene][name] = object;
                resolve();
            }.bind(this));
        });
    }

    async loadGTlf() {
        const gtlfNumber = Object.getOwnPropertyNames(this.gtlf).length;
        if (gtlfNumber > 0) {
            const loader = new THREE.GLTFLoader();
            for (const name in this.gtlf) {
                if (this.gtlf.hasOwnProperty(name)) {
                    const path = this.gtlf[name].path;
                    const scene = this.gtlf[name].scene ? this.gtlf[name].scene : "common";
                    await this._loadSyngleGTlf(loader, path, scene, name);
                    delete this.gtlf[name];
                }
            }
        }
    }

    async _loadSyngleGTlf(loader, path, scene, name) {
        return new Promise((resolve, reject) => {
            loader.load(path, function (object) {
                object.name = name;
                if (!this.resources[scene]) { //create scene resources object if not exists
                    this.resources[scene] = {};
                }
                this.resources[scene][name] = object.scene;
                resolve();
            }.bind(this),
            undefined, 
            function (error) {
                console.error(`GTLF loading error: ${error}`);
                reject();
            });
        });
    }

    async loadSounds() {
        const audioNumber = Object.getOwnPropertyNames(this.audio).length;
        if (audioNumber > 0) {
            const loader = new THREE.AudioLoader();
            for (const name in this.audio) {
                if (this.audio.hasOwnProperty(name)) {
                    const path = this.audio[name].path;
                    const scene = this.audio[name].scene ? this.audio[name].scene : "common";
                    await new Promise((resolve) => {
                        loader.load(path, function (object) {
                            object.name = name;
                            if (!this.resources[scene]) //create scene resources object if not exists
                                this.resources[scene] = {};
                            this.resources[scene][name] = object;
                            delete this.audio[name];
                            resolve();
                        }.bind(this));
                    });
                }
            }
        }
    }

    async loadTextures() {
        const texturesNumber = Object.getOwnPropertyNames(this.textures).length;
        if (texturesNumber > 0) {
            const loader = new THREE.TextureLoader();
            for (const name in this.textures) {
                if (this.textures.hasOwnProperty(name)) {
                    const path = this.textures[name].path;
                    const scene = this.textures[name].scene ? this.textures[name].scene : "common";
                    await new Promise((resolve) => {
                        loader.load(path, function (object) {
                            object.name = name;
                            if (!this.resources[scene]) //create scene resources object if not exists
                                this.resources[scene] = {};
                            this.resources[scene][name] = object;
                            delete this.textures[name];
                            resolve();
                        }.bind(this));
                    });
                }
            }
        }
    }

    async loadAll() {
        await this.loadFbx();
        await this.loadGTlf();
        await this.loadSounds();
        await this.loadTextures();
    }

    //returns true if resource had already been preloaded or false if not
    hasResource(sceneName, name) {
        let resourceFound = false;
        sceneName = sceneName ? sceneName : "common";
        if (this.resources.hasOwnProperty(sceneName) &&
            this.resources[sceneName].hasOwnProperty(name)) {
            if (!(this.resources[sceneName][name] instanceof Array) ||
                (this.resources[sceneName][name] instanceof Array &&
                    this.resources[sceneName][name].length > 0))
                resourceFound = true;
        }
        return resourceFound;
    }

    //returns loaded resource if it exists
    getResource(sceneName, name) {
        sceneName = sceneName ? sceneName : "common";
        if (this.resources.hasOwnProperty(sceneName) &&
            this.resources[sceneName].hasOwnProperty(name)) {
            if (isArray(this.resources[sceneName][name]) &&
                this.resources[sceneName][name].length > 0) {
                return this.resources[sceneName][name].pop();
            } else if (isArray(this.resources[sceneName][name]) &&
                this.resources[sceneName][name].length <= 0) {
                throw new Error(`MyLoader: array of resources ${name} has no elements to pop`);
            } else {
                return this.resources[sceneName][name];
            }
        } else {
            throw new Error(`MyLoader: resource ${name} not found for scene ${sceneName}`);
        }
    }

    //dispose everything
    dispose() {
        for (const sceneName in this.resources) {
            this.disposeSceneResources(sceneName)
        }
    }

    //dispose scene resources
    disposeSceneResources(sceneName) {
        if (this.resources.hasOwnProperty(sceneName)) {
            const sceneResources = this.resources[sceneName];
            for (const resourceName in sceneResources) {
                if (sceneResources.hasOwnProperty(resourceName)) {
                    const resource = sceneResources[resourceName];
                    if (resource.dispose)
                        resource.dispose();
                    else {
                        if (resource.geometry)
                            resource.geometry.dispose();
                        if (resource.material)
                            resource.material.dispose();
                    }
                    delete this.resources[sceneName][resourceName];
                }
            }
            delete this.resources[sceneName];
        }
    }

}