import App from "./app";

try {
    window.$ = window.jQuery = require('jquery');
    const loadTouchEvents = require('jquery-touch-events');
    loadTouchEvents($);
} catch (e) {}


$(function () {

    

    const app = new App(true);

    const camera = app.scenesManager.currentScene.camera;
    
    const x = camera.position.x;
    const y = camera.position.y;
    const z = camera.position.z;

    const fov = camera.fov;
    const near = camera.near;
    const far = camera.far;

    const $x = $("#x");
    $x.val(x);
    const $y = $("#y");
    $y.val(y);
    const $z = $("#z");
    $z.val(z);

    const $fov = $("#fov");
    $fov.val(fov)
    const $near = $("#near");
    $near.val(near);
    const $far = $("#far");
    $far.val(far);

    $x.on('change', (e) => {
        const x = $x.val();
        const y = camera.position.y;
        const z = camera.position.z;
        camera.position.set(x, y, z);
        camera.lookAt(0, 0, 0);
        camera.updateProjectionMatrix();
    });

    $y.on('change', (e) => {
        const x = camera.position.x;
        const y = $y.val();
        const z = camera.position.z;
        camera.position.set(x, y, z);
        camera.lookAt(0, 0, 0);
        camera.updateProjectionMatrix();

    });

    $z.on('change', (e) => {
        const x = camera.position.x;
        const y = camera.position.y;
        const z = $z.val();
        camera.position.set(x, y, z);
        camera.lookAt(0, 0, 0);
        camera.updateProjectionMatrix();
    });

    $fov.on('change', () => {
        const fov = $fov.val();
        camera.fov = fov;
        camera.lookAt(0, 0, 0);
        camera.updateProjectionMatrix();
    });

    $near.on('change', () => {
        const near = $near.val();
        camera.near = near;
        camera.lookAt(0, 0, 0);
        camera.updateProjectionMatrix();
    });

    $far.on('change', () => {
        const far = $far.val();
        camera.far = far;
        camera.lookAt(0, 0, 0);
        camera.updateProjectionMatrix();
    });
});